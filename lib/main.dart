import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "psu trang",
      theme: ThemeData(primarySwatch: Colors.lime),//สีบาร์
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.ac_unit_sharp),//อิโมจิ
          title: Text('My First App'),
          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.abc_rounded)),
                    IconButton(onPressed: (){}, icon: Icon(Icons.adb))],
        ),
        body: Center(
          child: Column(
          children: [
            CircleAvatar(
              backgroundImage: AssetImage('assets/20798.jpg'),
              radius: 130,
            ),
            // Image.asset('assets/20798.jpg'),
            Text(
              'นาย ณัฐชนน มณียม',
              style: TextStyle(height: 2, fontSize: 30 ,fontWeight: FontWeight.bold),
            ),
            Text(
              '6350110004',
              style: TextStyle(height: 2, fontSize: 20 ,fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      ),
    );
  }
}
//[]มีลูกได้หลายคน ()ได้คนเดียว